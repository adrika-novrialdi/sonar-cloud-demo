package id.ac.ui.cs.advprog.tutorial2.observer.repository;

import id.ac.ui.cs.advprog.tutorial2.observer.core.Quest;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class QuestRepository {
        private Map<String, Quest> quests = new HashMap<>();

        public Map<String, Quest> getQuests() {
                return quests;
        }

        public Quest save(Quest savedQuest) {
                Quest existingQuest = quests.get(savedQuest.title);
                if (existingQuest == null) {
                        quests.put(savedQuest.title, savedQuest);
                        return savedQuest;
                } else {
                        return null;
                }
        }
}
